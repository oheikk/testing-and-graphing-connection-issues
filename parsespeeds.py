#!/bin/python

import matplotlib.pyplot as plt
import sys

speeddata_file = sys.argv[1]

with open(speeddata_file, "r") as file:
    speeddata = file.read().split('\n')

speeds=[]

def parseForSpeed(line):
    # sometimes the text has random crap in it e.g. when speedgraph.sh is interrupted with ctrl-C
    # try-catch: let's not crash completely when that happens
    try:
        line = line[31:40].strip()
        speed = 0
        #print(line)
        if "M" in line:
            speed = float(line[0:len(line)-1])
        elif "k" in line:
            speed = float(line[0:len(line)-1])/1000
        else:
            speed = float(line)/1000000
    
        speeds.append(speed)
    except ValueError:
        return


# ugly hard-coded indices to parse the human-readable output from curl to a list of floats
for i in range(len(speeddata)):
    if "% Total" in speeddata[i]:

        # handle cases where download was interrupted by curl because it took too long
        if "curl" in speeddata[i-3]:
            parseForSpeed(speeddata[i-4])

        else:
            parseForSpeed(speeddata[i-3])
 

print(speeds)

slows = 0

for speed in speeds:
    if speed < 1.0:
        slows += 1

print("Total downloads in sample:", len(speeds))
print("Speed below 1MB/s ratio:", 100*slows/len(speeds),'%')
print("Average speed:", sum(speeds)/len(speeds))

plt.ylim(0,22)
plt.xlabel('Yksittäisiä 100MB tiedoston latauksia')
plt.ylabel('Latausnopeus (MB/s)')
plt.plot(speeds)
plt.xticks(range(len(speeds)))
plt.show()

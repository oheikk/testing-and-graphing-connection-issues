#!/bin/bash
# $1 = filename to save results to

if [ "$#" -ne 1 ]; then
	exit 1
fi

#links=("https://speed.hetzner.de/100MB.bin" "http://ipv4.download.thinkbroadband.com/100MB.zip" "http://54.36.162.206/static/etmain/cust-mp-v2.pk3" "https://mirror.kumi.systems/zorinos/16/Zorin-OS-16-Lite-64-bit.iso")
#links=("https://proof.ovh.net/files/100Mb.dat")
#links=("http://54.36.162.206/static/etmain/cust-mp-v2.pk3")
#links=("https://speed.hetzner.de/100MB.bin")
links=("http://ipv4.download.thinkbroadband.com/100MB.zip")


numlinks=${#links[@]}

i=0
while true
do
	indx=$(($i % $numlinks))
	currentLink="${links[$indx]}"
	
	echo $currentLink >> $1
	echo $currentLink
	date >> $1

	# download and discard file, print output of stderr to stdout and to write to filename
	# (curl puts info about download speed to stderr)
	# give up and move on if speed remains slow for 10 seconds
	# (also tried being much more patient than 10 seconds, doesn't make a difference)
	curl $currentLink --speed-limit 2000000 --speed-time 10 -o /dev/null 2> >(tee -a $1 >&2)

	i=$((i+1))
done
